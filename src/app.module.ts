import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TemperatureModule } from './temperature/temperature.module';
import { UsersModule } from './users/users.module';
@Module({
  imports: [TemperatureModule, UsersModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
//app.module ตัวหลักที่เขื่อมทุกอย่างเข้าด้วยกัน
//app.module ประกอบด้วย controller กับ service
//ใน netjs ตัวหลัก ๆ  คือ controller
