import { Injectable } from '@nestjs/common';

@Injectable()
//class นี้ สามารถเสียบเข้าไป
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }
}
