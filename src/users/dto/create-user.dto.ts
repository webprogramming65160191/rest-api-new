export class CreateUserDto {
  login: string;
  password: string;
  roles: ('admin' | 'User')[];
  gender: 'male' | 'female';
  age: number;
}
