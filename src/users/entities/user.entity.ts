export class User {
  id: number;
  login: string;
  password: string;
  roles: ('admin' | 'User')[];
  gender: 'male' | 'female';
  age: number;
}
